library(RODBC)
library(dplyr)
library(data.table)
library(ggplot2)
library(MatchIt)
library(gridExtra)

con <- odbcConnect('DSNColumbia', uid = 'Columbia', pwd = 'C_login1')

sqlColumns(con, "ElinEvents")

features <- sqlQuery(con,"SELECTm.EventID, m.EventType, m.StructureID, m.EventDate, m.StructureMapped, m.SVFound, m.BTicket, m.BTicketTimeCalledIn, m.BTicketTimeRelieved, m.BTicketVoltage, m.HarmonicsReading, m.VltFloodW1FOGrating,  m.CblContW1FOGrating, m.VoltageFrgGrnd, m.LadderOrCageRemoved FROM ManualTestingHistory m ")





# sql query
events <- sqlQuery(con,"select * from ElinEvents where year(IncidentDateTime) > 2009") %>% as.data.table
autoProg <- sqlQuery(con, "select * from EneEsr where year(EventDate) > 2009") %>% as.data.table
facilities <- sqlQuery(con, "select * from Facilities") %>% as.data.table

autoProg[, `:=`(EventDate = as.Date(EventDate))] # no times recorded, so no point it doing datetime, just date

svt <- autoProg[grepl("Stray Voltage Testing", ReasonForVisit)]
svt[,`:=`(Boro = as.character(Boro))]
facilities[,`:=`(Borough = as.character(Borough))]

assetMatchLoc <- function(latitude, longitude, borough){
  targets = facilities[Borough == borough,.(Latitude, Longitude, FacilityKey)]
  targets = targets[complete.cases(targets)]
  if(nrow(targets) == 0){
    return(-1L)
  } else {
    targets[,`:=`(lat = Latitude - latitude, lon = Longitude - longitude)]
    targets[, `:=`(diff = lat^2 + lon^2)]
    result = targets[which.min(targets[,diff]), FacilityKey]
    if(length(result) == 1){
      return(result)
    } else{
      return(-1L)
    }
  }
}
# associates facility with marked targets
svt[,`:=`(closestFacKey = assetMatchLoc(Latitude, Longitude, Boro)), by = 1:nrow(svt)]

# get testing dates -> can't do that for all data
test_dates = svt[format(EventDate, "%Y") == 2011,EventDate] %>% unique
all_facility_keys = facilities[,FacilityKey]
data = merge(test_dates, all_facility_keys, all = True) # cross_join

# test on one year
comp_feat <- sqlQuery(con,"SELECT m.EventID, m.EventType, m.StructureID, m.EventDate, m.StructureMapped, m.SVFound, m.BTicket, m.BTicketTimeCalledIn, m.BTicketTimeRelieved, m.BTicketVoltage, m.HarmonicsReading, m.VltFloodW1FOGrating,  m.CblContW1FOGrating, m.VoltageFrgGrnd, m.LadderOrCageRemoved FROM ManualTestingHistory m ") %>% as.data.table
svt <- comp_feat
svt$latestincidentdate = as.Date(svt$latestincidentdate)

# build train data
test_dates = svt[, latestincidentdate] %>% unique
test_dates = as.Date(test_dates[complete.cases(test_dates)])
fac_keys = svt[,FacilityKey] %>% unique
data = merge(test_dates, fac_keys, all = True) %>% as.data.table
colnames(data) <- c("latestincidentdate", "FacilityKey")
d1 <- merge(data, svt, by = c("latestincidentdate", "FacilityKey"), all.x = T)

#target_facilities <- facilities[,.(FacilityKey, Latitude, Longitude)]
#target_facilities <- target_facilities[complete.cases(target_facilities),]
#d1 <- merge(d1, target_facilities, by = "FacilityKey")
#weather <- comp_feat[Borough == "M"] %>% dplyr::select(latestincidentdate, Weather) %>% na.omit
#weather$latestincidentdate <- as.Date(weather$latestincidentdate)
#weather$Weather <- as.character(weather$Weather)
#weather <- weather %>% group_by(latestincidentdate) %>% summarize(Weather = min(Weather))
#d1 <- merge(d1, weather, by = "latestincidentdate")
#d1$Weather <- as.factor(d1$Weather)

# propensity score matching
mod1 <- glm(Mobile_test_program ~ Latitude + Longitude + 
              eventdate + latestincidentdate, family = "binomial", data = d1  )
mod_match <- matchit(Mobile_test_program ~ latestincidentdate + Latitude + Longitude + eventdate, method = "nearest", data = d1)
matched <- match.data(mod_match)

# graphing
grid.arrange(
  ggplot(data = matched, aes(x = distance, y = latestincidentdate, color = Mobile_test_program)) + geom_point() + geom_smooth(method = "loess"),
  ggplot(data = matched, aes(x = distance, y = eventdate, color = Mobile_test_program)) + geom_point() + geom_smooth(method = "loess"),
  ggplot(data = matched, aes(x = distance, y = Latitude, color = Mobile_test_program)) + geom_point() + geom_smooth(method = "loess"),
  ggplot(data = matched, aes(x = distance, y = Longitude, color = Mobile_test_program)) + geom_point() + geom_smooth(method = "loess"),
  nrow = 4)
# estimate treatment effect
matched$diff <- matched$qa_1yr_after_man_stray - matched$qa_1yr_prior_man_stray
with(matched, t.test(diff ~ Mobile_test_program))

