--select distinct VentedCover
--FROM [Columbia].[dbo].[ventedCovers]

--select StructureType, StructureNumber, Borough, MSPlate FROM [Columbia].[dbo].[Facilities]

--F1: [Facilities], 246638 records
--AFS: AssetIdtoFacilityKey, 559510 records, AFS is more than 2 times larger than F1
--F: F1.FacilityKey = AFS.FacilityKey
--VC: Ventedcovers, 113935 records
--FVC: F.StructureId = VC.StructureID
--MTH1: ManualTestingHistory, 2545581 records
--MTH2:  StructureID, maxEventDate FROM MTH1 GROUP BY StructureID
--MTH: MTH1.StructureID = MTH2.StructureID AND MTH1.EventDate = MTH2.maxEventDate
--FVC.StructureId = MTH.StructureID


SELECT FVC.AssetID, FVC.FacilityKey, FVC.StructureId, FVC.StructureType, FVC.StructureNumber,
		FVC.Borough, FVC.MSPlate, FVC.CoverShape, FVC.CoverType, MTH.Weather, MTH.GroundCondition
FROM 
	(SELECT F.AssetId, F.FacilityKey, F.StructureId, F.StructureType, F.StructureNumber,
			F.Borough, F.MSPlate, VC.CoverShape, VC.CoverType, VC.NumOfCovers
	FROM
		(SELECT F1.AssetId, /*AFS.AssetId, AFS.FacilityKey,*/ F1.FacilityKey, AFS.StructureId, F1.StructureType, F1.StructureNumber,
				F1.Borough, F1.MSPlate
			FROM
				[Columbia].[dbo].[Facilities] AS F1
				LEFT OUTER JOIN
				[Columbia].[dbo].[AssetIdtoFacilityKey] AS AFS
				ON --F1.AssetId = AFS.AssetId
				   F1.FacilityKey = AFS.FacilityKey
			) AS F
	LEFT OUTER JOIN
		-- TODO: investigate small number of possible duplicates
		(SELECT * FROM [Columbia].[dbo].[ventedCovers]) AS VC
	ON /*F.StructureType = VC.StructureType
	   AND
	   F.StructureNumber = VC.StructureNumber
	   AND
	   F.MSPlate = VC.MSPlate*/
	   /*F.FacilityKey = VC.facilityKey
	   OR F.AssetId = VC.AssetID
	   OR*/ F.StructureId = VC.StructureID) AS FVC
LEFT OUTER JOIN
	(SELECT MTH1.*
		FROM
			[Columbia].[dbo].[ManualTestingHistory] AS MTH1
			INNER JOIN
			(SELECT StructureID, max(EventDate) maxEventDate FROM [Columbia].[dbo].[ManualTestingHistory] GROUP BY StructureID) MTH2
		ON
			MTH1.StructureID = MTH2.StructureID
			AND MTH1.EventDate = MTH2.maxEventDate) AS MTH
ON
	FVC.StructureId = MTH.StructureID

