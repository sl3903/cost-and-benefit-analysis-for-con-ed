SELECT cf.*,insp_count
From compiled_features as cf
left outer join 
(Select QA.ASST_ID, count(1) insp_count from QA
group by ASST_ID) as qa
on qa.ASST_ID=cf.AssetID