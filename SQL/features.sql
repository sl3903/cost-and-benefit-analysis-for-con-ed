Create View easymedium as
SELECT property.*,failurehistory.incidentborough,failurehistory.latestincidentdate,failurehistory.TroubleType
From
(
 
   Select easyfeatures.*,Cableproperty.out_cables,Cableproperty.in_cables,Cableproperty.total_cables,Cableproperty.oldest_cable_built_date,Cableproperty.ConductorMaterial_CU,Cableproperty.ConductorMaterial_AL
    from easyfeatures
   INNER JOIN Cableproperty
   on easyfeatures.FacilityKey=Cableproperty.FacilityKey
  
) as property
LEFT OUTER JOIN 
(
SELECT incident.*,ElinEvents.[Borough] as incidentborough,ElinEvents.[TroubleType],ElinEvents.[IncidentDateTime],ElinEvents.[StructureType]
From
( SELECT [FacilityKey],max([IncidentDateTime]) latestincidentdate
  From ElinEvents
  GROUP BY FacilityKey
) as incident
  INNER JOIN ElinEvents
  on incident.FacilityKey=ElinEvents.FacilityKey


) as failurehistory
on property.FacilityKey=failurehistory.FacilityKeys