--C_F: Cable Frome
--C_T: Cable To
--Insulation Material: too many 0's and Null's
--Conductor Materials: enough data but not distinct for each group, count it
Create View Cableproperty
 as
 SELECT cables.*, C_M.ConductorMaterial_CU,C_M.ConductorMaterial_AL,out_cables-ConductorMaterial_AL-ConductorMaterial_CU as ConductorMaterial_NULL
 From
(SELECT C_F.FromFacilityKey as FacilityKey, C_F.FromFacilityAssetId AS AssetId, C_F.out_cables, C_T.in_cables,
		C_F.out_cables + C_T.in_cables AS total_cables,
	CASE 
		WHEN C_F.oldest_out_cbl is NULL THEN C_T.oldest_in_cbl
		WHEN C_T.oldest_in_cbl  is NULL THEN C_F.oldest_out_cbl
		WHEN C_T.oldest_in_cbl >= C_F.oldest_out_cbl THEN C_F.oldest_out_cbl
		ELSE C_T.oldest_in_cbl
	END AS oldest_cable_built_date
FROM 
	
	(select FromFacilityKey, FromFacilityAssetId, count(1) out_cables, min(BuiltDate) oldest_out_cbl
	FROM [Columbia].[dbo].[Cables]
	GROUP BY FromFacilityKey, FromFacilityAssetId) C_F
INNER JOIN
	(select ToFacilityKey, ToFacilityAssetId, count(1) in_cables, min(BuiltDate) oldest_in_cbl
	FROM [Columbia].[dbo].[Cables]
	GROUP BY ToFacilityKey, ToFacilityAssetId) C_T
ON C_F.FromFacilityKey = C_T.ToFacilityKey
) as cables
inner join
(
Select CU.FacilityKey,CU.AssetId,CU.ConductorMaterial_CU,
case 
    when AL.ConductorMaterial_AL is NULL then 0
	else AL.ConductorMaterial_AL
	end as ConductorMaterial_AL
From
(SELECT FromFacilityKey as FacilityKey, FromFacilityAssetId AS AssetId, count(1) as ConductorMaterial_CU
From Cables
where ConductorMaterial='CU'
group by FromFacilityKey,FromFacilityAssetId) as CU
full outer join
(SELECT FromFacilityKey as FacilityKey, FromFacilityAssetId AS AssetId, count(1) as ConductorMaterial_AL
From Cables
where ConductorMaterial='AL'
group by FromFacilityKey,FromFacilityAssetId) as AL
on CU.FacilityKey=AL.FacilityKey and CU.AssetId=AL.AssetId
where CU.FacilityKey is not NULL
--order by FacilityKey
) as C_M
on cables.FacilityKey=C_M.FacilityKey
order by FacilityKey




