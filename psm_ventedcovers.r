library(RODBC)
library(Matching)
library(MatchIt)
library(DMwR)
library(dplyr)
library(gbm)
library(ggplot2)
library(optmatch)
library(gridExtra)
library(mefa)

mode <- "VENTED" #'VENTED_NBRS'
cutoff_yr <- 2013
# separte model for each borough
boro <- "B"
num_trees <- 1500

con <- odbcConnect('costbenefit', uid = 'Columbia', pwd = 'C_login1')

# query events data and feature set
features <- sqlQuery(con,"select * from compiled_features_20161221_1724")
elin_events <- sqlQuery(con,"select * from ElinEvents_processed")
ene_events <- sqlQuery(con, "select * from EneEsr_processed")
close(con)

# load neighbor data
#adj_data <- read.csv("F:/Documents/adjacency_data.csv", header=FALSE)

# add year columns
elin_events$event_yr <- as.numeric(format(elin_events$IncidentDateTime, "%Y"))
ene_events$event_yr <- as.numeric(format(ene_events$EventDate , "%Y"))
features$vent_yr <- ifelse(is.na(features$vent_dt), NA, as.numeric(format(features$vent_dt, "%Y")))

# add oldest cable age
features$oldest_cable_age = 2016 - as.numeric(format(features$oldest_cable_built_date, "%Y"))

# set NA's to 0 for aggregated event columns
columns <- colnames(features)
for (ix in union(grep("event", columns, ignore.case = TRUE),
                 grep("evnt", columns, ignore.case = TRUE))) {
  print(columns[ix])
  write(paste0(sprintf("features$%s[is.na(features$%s)]", columns[ix], columns[ix]), " <- 0"), "C:/Columbia/test/tmp.R", append=TRUE)
}
source("C:/Columbia/test/tmp.R")
file.remove("C:/Columbia/test/tmp.R")

features$vented_cover[is.na(features$vented_cover)] <- 0

# event counts
elin_events_filtered <- subset(elin_events, event_yr >= cutoff_yr)
elin_major_filtered <- subset(elin_events, event_yr >= cutoff_yr & (TroubleType == "MHX" |
                                                                 TroubleType =="MHF"))
  
#elin_shortlist <- merge(features_b_filtered, elin_events_filtered, by="FacilityKey")
elin_all <- table(elin_events_filtered$FacilityKey)
elin_mjr <- table(elin_major_filtered$FacilityKey)

ene_events_filtered <- subset(ene_events, event_yr >= cutoff_yr)
ene_all <- table(ene_events_filtered$Closest_FacilityKey)



  
  features_b <- features[features$Borough == boro,]
  #print(length(features_b))

  # drop vented data from 2013 onwards
  features_b_filtered <- subset(features_b, is.na(vent_dt) | vent_yr < cutoff_yr)
  features_b_dropped <- subset(features_b, vent_yr >= cutoff_yr)
  
  if (mode == 'VENTED_NBRS') {
    
    # find adjacent structures structures
    #features_b_dropped_keys <- features_b_dropped[, c("FacilityKey")]
    #dropped_nbrs <- merge(features_b_dropped[, c("FacilityKey")], adjacency_data, by="FacilityKey"
    drop_nbrs <- adjacency_data[adjacency_data$FacilityKey %in% features_b_dropped$FacilityKey,]
    drop_nbrs_keys <- c(na.omit(as.character(c(drop_nbrs, recursive=TRUE))))
    
    incl_nbrs <- adjacency_data[adjacency_data$FacilityKey %in% features_b_filtered$FacilityKey,]
    incl_nbrs_keys <- c(na.omit(as.character(c(incl_nbrs, recursive=TRUE))))
    
    # set neighbors of vented structures to vented, if not already so
    features_b_filtered[features_b_filtered$FacilityKey %in% incl_nbrs_keys, c("vented_cover")] <- 1
    # drop facility keys for structure vented after cutoff and their neighbours which were
    # not already neihbors of structures vented before cutoff
    fkeys_to_drop <- setdiff(drop_nbrs_keys, incl_nbrs_keys)
    features_b_filtered <- subset(features_b_filtered, !(FacilityKey %in% fkeys_to_drop))

  }

  features_b_filtered <- merge(features_b_filtered, as.data.frame(elin_all),
                               by.x="FacilityKey", by.y = "Var1", all.x = TRUE)
  names(features_b_filtered)[names(features_b_filtered) == "Freq"] <- "all_elin"
  features_b_filtered$all_elin[is.na(features_b_filtered$all_elin)] <- 0
  
  features_b_filtered <- merge(features_b_filtered, as.data.frame(elin_mjr),
                               by.x="FacilityKey", by.y = "Var1", all.x = TRUE)
  names(features_b_filtered)[names(features_b_filtered) == "Freq"] <- "mjr_elin"
  features_b_filtered$mjr_elin[is.na(features_b_filtered$mjr_elin)] <- 0
  
  features_b_filtered <- merge(features_b_filtered, as.data.frame(ene_all),
                               by.x="FacilityKey", by.y = "Var1", all.x = TRUE)
  names(features_b_filtered)[names(features_b_filtered) == "Freq"] <- "all_ene"
  features_b_filtered$all_ene[is.na(features_b_filtered$all_ene)] <- 0

  #Tr_b <- features_b_filtered$vented_cover
  
  # balance check
  table(features_b_filtered$vented_cover)
  n_pos <- nrow(subset(features_b_filtered, vented_cover == 1))
  cat("positive", n_pos)
  n_neg <- nrow(features_b_filtered) - n_pos
  cat(", negative", n_neg)
  
  if (n_pos > 3.0*n_neg) {
    #test1 <- NA
    #assign(test1, features_b_filtered)
    # drop date columns
    # test_sample <- subset(features_b_filtered, select = -c(oldest_cable_built_date, vent_dt, latestincidentdate, qa_INSP_DATE, sp_work_finish, MSPlate))
    # test_sample$vented_cover <- as.factor(test_sample$vented_cover)
    # test_sample$StructureType <- as.factor(test_sample$StructureType)
    # test_sample$CoverType <- as.factor(test_sample$CoverType)
    # test_sample$CoverShape <- as.factor(test_sample$CoverShape)
    # test_sample$Weather <- as.factor(test_sample$Weather)
    # test_sample$GroundCondition <- as.factor(test_sample$GroundCondition)
    # test_sample$TroubleType <- as.factor(test_sample$TroubleType)
    # test_sample$Network1 <- as.factor(test_sample$Network1)
    # test_sample$Network2 <- as.factor(test_sample$Network2)
    # 
    # test_ovrsampl <- SMOTE(vented_cover ~ out_cables + total_cables + total_cu + total_al + neutral_mains
    #                        + main_phase_cbls + srvc_phase_cbls + nwk_count #+ CoverShape + CoverType
    #                        + all_events + all_COpresent_events + lastyear_events + lastyear_COpresent_events
    #                        + last3year_events + last3year_COpresent_events + all_major_events
    #                        + all_COpresent_major_events + lastyear_major_events + lastyear_COpresent_major_events
    #                        + last3year_major_events + last3year_COpresent_major_events #+ oldest_cable_built_date
    #                        + QA_program + MTH_program + Insp_program + Mobile_test_program
    #                        + Latitude + Longitude + Total_MH + Vented_MH + oldest_cable_age,
    #                        data=test_sample, perc.over = 100, #100.0*n_pos/n_neg,
    #                        k=10, perc.under = 200)#100.0*n_neg/n_pos)
    
    pos_subset = test_sample[sample(nrow(test_sample), n_neg),]
    test_undersampl <- rbind(pos_subset, subset(test_sample, vented_cover == 0))
    features_b_filtered <- test_undersampl
  }
  
  # Ene + Elin events
  #Y_b <- features_b_filtered$Freq #vc_1yr_after_all_evnts

  features_b_filtered %>%
    group_by(vented_cover) %>%
    summarize(num_structures = n(),
              avg_all_1yr_pr = mean(vc_1yr_prior_all_evnts),
              se_pr = sd(vc_1yr_after_all_evnts)/sqrt(num_structures),
              avg_all_1yr_aftr = mean(vc_1yr_after_all_evnts),
              se_aftr = sd(vc_1yr_after_all_evnts)/sqrt(num_structures))

  with(features_b_filtered, t.test(all_elin ~ vented_cover))

  cov_b <- c('out_cables', 'total_cables', 'neutral_mains', 'main_phase_cbls', 
             'srvc_phase_cbls', 'nwk_count', 'Vented_MH', 'Total_MH', 'total_cu',
             'total_al', 'all_events', 'all_COpresent_events', 'lastyear_events',
             'lastyear_COpresent_events', 'last3year_events', 'last3year_COpresent_events',
             'all_major_events', 'all_COpresent_major_events')#, 'Longitude', 'Latitude', 'CoverShape', 'CoverType')
  features_b_filtered %>%
    group_by(vented_cover) %>%
      select(one_of(cov_b)) %>%
        summarise_all(funs(mean(., na.rm = TRUE)))

  # lapply(cov_b, function(v) {
  #   t.test(features_b_filtered[, v] ~ features_b_filtered[, 'vented_cover'])
  # })
  
  for (cov in cov_b) {
    #par(mfrow=c(1,2))
    #rng <- range(features_b_filtered[, c(cov)], na.rm = TRUE)
    # hist(features_b_filtered[features_b_filtered$vented_cover == 0, c(cov)], xlim = rng,
    #      xlab = cov, main = "control")
    # hist(features_b_filtered[features_b_filtered$vented_cover == 1 , c(cov)], xlim = rng,
    #      xlab = cov, main = "treated")
    ggplot(aes_string(x = cov), data = features_b_filtered[, c("vented_cover", cov)]) + 
        geom_histogram() + facet_wrap(~ vented_cover)
  }
  
  #rng <- range(features_b_filtered[, c("all_COpresent_major_events")], na.rm = TRUE)
  ggplot(aes_string(x = "all_COpresent_major_events"), data = features_b_filtered[, c("vented_cover", "all_COpresent_major_events")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)
  ggplot(aes_string(x = "out_cables"), data = features_b_filtered[, c("vented_cover", "out_cables")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)
  ggplot(aes_string(x = "total_cables"), data = features_b_filtered[, c("vented_cover", "total_cables")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)
  ggplot(aes_string(x = "main_phase_cbls"), data = features_b_filtered[, c("vented_cover", "main_phase_cbls")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)
  ggplot(aes_string(x = "neutral_mains"), data = features_b_filtered[, c("vented_cover", "neutral_mains")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)
  ggplot(aes_string(x = "srvc_phase_cbls"), data = features_b_filtered[, c("vented_cover", "srvc_phase_cbls")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)
  ggplot(aes_string(x = "Vented_MH"), data = features_b_filtered[, c("vented_cover", "Vented_MH")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)
  ggplot(aes_string(x = "all_events"), data = features_b_filtered[, c("vented_cover", "all_events")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)
  ggplot(aes_string(x = "all_major_events"), data = features_b_filtered[, c("vented_cover", "all_major_events")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)

  

  
  if (mode == 'VENTED_NBRS') {
  
    gbm_b <- gbm(vented_cover ~ #out_cables #+ total_cables + total_cu  #+ neutral_mains + main_phase_cbls
                 #total_al + srvc_phase_cbls + nwk_count #+ CoverShape + CoverType
                 all_events + all_COpresent_events + lastyear_events + lastyear_COpresent_events
                 + last3year_events + last3year_COpresent_events + all_major_events
                 + all_COpresent_major_events + lastyear_major_events + lastyear_COpresent_major_events
                 + last3year_major_events + last3year_COpresent_major_events #+ oldest_cable_built_date
                 + QA_program + MTH_program + Insp_program + Mobile_test_program
                #+ vc_1yr_prior_evnts + vc_2yr_prior_evnts + vc_3yr_prior_evnts
                #+ vc_1yr_prior_all_evnts + vc_2yr_prior_all_evnts + vc_3yr_prior_all_evnts
                #+ vc_1yr_prior_insp_summ + vc_2yr_prior_insp_summ + vc_3yr_prior_insp_summ
                #+ vc_1yr_prior_sip + vc_2yr_prior_sip + vc_3yr_prior_sip
                #+ vc_1yr_prior_shock_evnts + vc_2yr_prior_shock_evnts + vc_3yr_prior_shock_evnts
                #+ vc_om_all + vc_om_1yr_prior_open + vc_om_2yr_prior_open + vc_om_3yr_prior_open
                + Total_MH + Vented_MH, #+ oldest_cable_age + Latitude + Longitude,
                distribution = "bernoulli",
                data = features_b_filtered,
                n.trees = 1000,
                shrinkage = 0.01,
                interaction.depth = 5,
                n.minobsinnode = 8,
                verbose = TRUE)
  } else {
    
    gbm_b <- gbm(vented_cover ~ out_cables + total_cables + total_cu + neutral_mains + main_phase_cbls
                 + total_al + srvc_phase_cbls + nwk_count #+ CoverShape + CoverType
                 + all_events + all_COpresent_events + lastyear_events + lastyear_COpresent_events
                 + last3year_events + last3year_COpresent_events + all_major_events
                 + all_COpresent_major_events + lastyear_major_events + lastyear_COpresent_major_events
                 + last3year_major_events + last3year_COpresent_major_events #+ oldest_cable_built_date
                 + QA_program + MTH_program + Insp_program + Mobile_test_program
                 #+ vc_1yr_prior_evnts + vc_2yr_prior_evnts + vc_3yr_prior_evnts + StructureType
                 #+ vc_1yr_prior_all_evnts + vc_2yr_prior_all_evnts + vc_3yr_prior_all_evnts
                 #+ vc_1yr_prior_insp_summ + vc_2yr_prior_insp_summ + vc_3yr_prior_insp_summ
                 #+ vc_1yr_prior_sip + vc_2yr_prior_sip + vc_3yr_prior_sip
                 #+ vc_1yr_prior_shock_evnts + vc_2yr_prior_shock_evnts + vc_3yr_prior_shock_evnts
                 #+ vc_om_all + vc_om_1yr_prior_open + vc_om_2yr_prior_open + vc_om_3yr_prior_open
                 + Total_MH + Vented_MH + oldest_cable_age + Latitude + Longitude,
                 distribution = "bernoulli",
                 data = features_b_filtered,
                 n.trees = num_trees,
                 shrinkage = 0.01,
                 interaction.depth = 5,
                 n.minobsinnode = 8,
                 verbose = TRUE)
  }

  #summary(gbm_b)
  #gbm.perf(gbm_b)
  
  trainpredict <- predict(object = gbm_b, newdata = features_b_filtered, type = "response",
          n.trees = num_trees)#gbm.perf(gbm_b))
  
  propensity_score <- data.frame(score=trainpredict, vented_cover=features_b_filtered$vented_cover)
  features_b_filtered$propensity_score <- propensity_score$score
  
  label = c("treated", "control")
  propensity_score %>% 
    mutate(vented_cover = ifelse(vented_cover == 1, label[1], label[2])) %>%
      ggplot(aes(x = score)) + geom_histogram() + facet_wrap(~vented_cover)
  
  # matching
  #Tr_b <- features_b_filtered$vented_cover
  cov_b_fewNA <- c("neutral_mains", "main_phase_cbls", "srvc_phase_cbls",
                   "all_events", "all_major_events", "last3year_events", "Latitude", "Longitude",
                   "all_COpresent_events", "last3year_COpresent_events", "last3year_major_events",
                   "all_COpresent_major_events", "nwk_count", "propensity_score", "all_elin",
                   "mjr_elin", "all_ene")
  features_b_noNA <- features_b_filtered %>%
    select(vented_cover, one_of(cov_b_fewNA)) %>%
      na.omit()
  
  matched <- matchit(vented_cover ~ neutral_mains + srvc_phase_cbls#+ main_phase_cbls, 
                     + all_events + all_major_events + last3year_events + Latitude
                     + Longitude + all_COpresent_events + last3year_COpresent_events
                     + last3year_major_events + all_COpresent_major_events
                     + nwk_count + propensity_score,
                     data = features_b_noNA, method = "nearest", distance = "mahalanobis",
                    reestimate=TRUE, replace=TRUE)

  # matched_genetic <- matchit(vented_cover ~ neutral_mains + main_phase_cbls + srvc_phase_cbls
  #                    + all_events + all_major_events + last3year_events,
  #                    data = features_b_noNA, method = "genetic", ties=FALSE,
  #                    verbose=TRUE, tolerance=0.01, distance.tolerance = 0.01,
  #                    pop.size = 20000, distance="mahalanobis")

  data_m <- match.data(matched)
  
  summary(matched)
  #plot(matched)

  # adjust weights
  match_pos <- nrow(subset(data_m, vented_cover == 1))
  match_neg <- nrow(data_m) - match_pos
  adj_weights <- c(as.numeric(c(subset(subset(data_m, vented_cover == 0), select=c("weights")),
                                recursive=TRUE)))*match_pos/match_neg
  # check match balance  
  for (cov in cov_b_fewNA) { #colnames(data_m)) {
    # par(mfrow=c(1,2))
    # rng <- range(features_b_filtered[, c(cov)], na.rm = TRUE)
    # hist(features_b_filtered[features_b_filtered$vented_cover == 0, c(cov)], xlim = rng,
    #      xlab = cov, main = "control")
    # hist(features_b_filtered[features_b_filtered$vented_cover == 1 , c(cov)], xlim = rng,
    #      xlab = cov, main = "treated")

    if (cov != "vented_cover") {
      # statistical test for matched data
      result <- ks.test(rep(as.numeric(c(subset(subset(data_m, vented_cover == 0), select=c(cov)), recursive=TRUE)),
                            adj_weights), 
                        as.numeric(c(subset(subset(data_m, vented_cover == 1), select=c(cov)), recursive=TRUE)))
      cat(cov)
      #result$data.name
      cat(", p-value: ", result$p.value, "\n")
    }
  }
  
  controls <- subset(data_m, vented_cover == 0)
  controls$weights <- controls$weights * match_pos/match_neg
  data_m <- rbind(controls, subset(data_m, vented_cover == 1))
  data_m <- mefa:::rep.data.frame(data_m,
                                  times=c(as.numeric(c(data_m$weights, recursive=TRUE))))
  
  ggplot(aes_string(x = "neutral_mains"), data = data_m[, c("vented_cover", "neutral_mains")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)
  ggplot(aes_string(x = "main_phase_cbls"), data = data_m[, c("vented_cover", "main_phase_cbls")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)
  ggplot(aes_string(x = "srvc_phase_cbls"), data = data_m[, c("vented_cover", "srvc_phase_cbls")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)
  ggplot(aes_string(x = "Latitude"), data = data_m[, c("vented_cover", "Latitude")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)
  ggplot(aes_string(x = "Longitude"), data = data_m[, c("vented_cover", "Longitude")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)
  ggplot(aes_string(x = "propensity_score"), data = data_m[, c("vented_cover", "propensity_score")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)
  ggplot(aes_string(x = "all_events"), data = data_m[, c("vented_cover", "all_events")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)
  ggplot(aes_string(x = "all_major_events"), data = data_m[, c("vented_cover", "all_major_events")]) + 
    geom_histogram() + facet_wrap(~ vented_cover)
  
  # data_m %>%
  #   group_by(vented_cover) %>%
  #     select(one_of(cov_b_fewNA)) %>%
  #       summarise_all(funs(mean))
  # 
  # check_bal <- function(data, var) {
  #   data$var <- data[, var]
  #   data$vented_cover <- as.factor(data$vented_cover)
  #   support <- c(min(data$var), max(data$var))
  #   ggplot(data, aes(x=distance, y=var, color=vented_cover)) +
  #            geom_point(alpha=0.2, size=1.3) +
  #            geom_smooth(method="loess", se=F) +
  #            xlab("propensity") +
  #            ylab(var) +
  #            ylim(support)
  # }
  # 
  # 
  # grid.arrange(
  #   check_bal(data_m, "neutral_mains"),
  #   check_bal(data_m, "srvc_phase_cbls"),
  #   check_bal(data_m, "all_events"),
  #   check_bal(data_m, "all_major_events"),
  #   ncol=2
  # )
  # 
  # grid.arrange(
  #   check_bal(data_m, "last3year_events"),
  #   check_bal(data_m, "main_phase_cbls"),
  #   #check_bal(data_m, "Latitude"),
  #   #check_bal(data_m, "Longitude"),
  #   check_bal(data_m, "all_COpresent_events"),
  #   check_bal(data_m, "last3year_COpresent_events"),
  #   check_bal(data_m, "last3year_major_events"),
  #   check_bal(data_m, "all_COpresent_major_events"),
  #   check_bal(data_m, "nwk_count"),
  #   ncol = 2
  #)
 
  # treatment effect
       
  effect_all <- lm(all_elin + all_ene ~ vented_cover, data=data_m)#, weights = data_m$weights)
  summary(effect_all)
  
  effect_elin <- lm(all_elin ~ vented_cover, data=data_m)#, weights = data_m$weights)
  summary(effect_elin)
  
  effect_ene <- lm(all_ene ~ vented_cover, data=data_m)#, weights = data_m$weights)
  summary(effect_ene)
  
  effect_mjr <- lm(mjr_elin ~ vented_cover, data=data_m)#, weights = data_m$weights)
  summary(effect_mjr)
  
  