summary(m.out3)

#summary(m.out3,interaction=T,addlvariables=NA, standerdize=T)

plot(m.out3)

plot(m.out3,type='jitter')

plot(m.out3,type='hist')

fml_te3=as.formula('(events_3year ) ~ StructureType + Borough + Latitude + 
                   Longitude + out_cables + in_cables + total_cables + oldest_cable_built_date + 
                   total_cu + total_al + neutral_mains + main_phase_cbls + srvc_phase_cbls + 
                   incidentborough + latestincidentdate + TroubleType + nwk_count + 
                   COPresent + all_events + all_COpresent_events + lastyear_events + 
                   lastyear_COpresent_events + last3year_events + last3year_COpresent_events + 
                   all_major_events + all_COpresent_major_events + lastyear_major_events + 
                   lastyear_COpresent_major_events + last3year_major_events + 
                   last3year_COpresent_major_events + Total_MH + Vented_MH + 
                   QA_program + MTH_program + vented_cover + Mobile_test_program + 
                   latestincidenttime + latestincidentyear + latestincidentmonth + 
                   latestincidentday + ocb_year + ocb_month + ocb_day + level1')

fml_te3_boxcox=as.formula('(events_3year+1) ~ StructureType + Borough + Latitude + 
                          Longitude + out_cables + in_cables + total_cables + oldest_cable_built_date + 
                          total_cu + total_al + neutral_mains + main_phase_cbls + srvc_phase_cbls + 
                          incidentborough + latestincidentdate + TroubleType + nwk_count + 
                          COPresent + all_events + all_COpresent_events + lastyear_events + 
                          lastyear_COpresent_events + last3year_events + last3year_COpresent_events + 
                          all_major_events + all_COpresent_major_events + lastyear_major_events + 
                          lastyear_COpresent_major_events + last3year_major_events + 
                          last3year_COpresent_major_events + Total_MH + Vented_MH + 
                          QA_program + MTH_program + vented_cover + Mobile_test_program + 
                          latestincidenttime + latestincidentyear + latestincidentmonth + 
                          latestincidentday + ocb_year + ocb_month + ocb_day + level1')


fml_te3_transformed=as.formula('(events_3year+1)^(-2) ~ StructureType + Borough + Latitude + 
                               Longitude + out_cables + in_cables + total_cables + oldest_cable_built_date + 
                               total_cu + total_al + neutral_mains + main_phase_cbls + srvc_phase_cbls + 
                               incidentborough + latestincidentdate + TroubleType + nwk_count + 
                               COPresent + all_events + all_COpresent_events + lastyear_events + 
                               lastyear_COpresent_events + last3year_events + last3year_COpresent_events + 
                               all_major_events + all_COpresent_major_events + lastyear_major_events + 
                               lastyear_COpresent_major_events + last3year_major_events + 
                               last3year_COpresent_major_events + Total_MH + Vented_MH + 
                               QA_program + MTH_program + vented_cover + Mobile_test_program + 
                               latestincidenttime + latestincidentyear + latestincidentmonth + 
                               latestincidentday + ocb_year + ocb_month + ocb_day + level1')

bootstrap=function(treatment_group,control_group,formular)
{ 
  t=sample(dim(treatment_group)[1],dim(treatment_group)[1],replace = T)
  c=sample(dim(control_group)[1],dim(control_group)[1],replace = T)
  data=rbind(treatment_group[t,],control_group[c,],rep(c(1,0),level1=c(dim(treatment_group)[1],dim(control_group)[1])))
  lrm=lm(formular,data = data)
  print(lrm$coefficients[length(lrm$coefficients)])
  lrm$coefficients[length(lrm$coefficients)]
}
n_boot=1000
result_boot3=c()
result_boot3_transformed=c()
for(i in 1:n_boot)
{
  result_boot3[i]=bootstrap(match.data(m.out3,'treat'),match.data(m.out3,'control'),fml_te3)
  result_boot3_transformed[i]=bootstrap(match.data(m.out3,'treat'),match.data(m.out3,'control'),fml_te3_transformed)
  
}

lrm3=lm(fml_te3,data = match.data(m.out3))
summary(lrm3)
hist(residuals(lrm3),xlab = 'residuals',main='Residual Plot')
qqnorm(residuals(lrm3),main = 'QQplot of the Residuals')
qqline(residuals(lrm3))


boxcox(lm(fml_te3_boxcox,data = match.data(m.out3)))
lrm3_transformed=lm(fml_te3_transformed,data = match.data(m.out3))
summary(lrm3_transformed)

hist(result_boot3,xlab = 'treatment effect',main = 'Histogram of Treatment effect')
abline(v=quantile(result_boot3,c(0.025,0.975)),lty=2)
quantile(result_boot3,c(0.025,0.5,0.975))

hist(result_boot3_transformed,xlab = 'treatment effect',main = 'Histogram of Transformed Treatment effect')
abline(v=quantile(result_boot3_transformed,c(0.025,0.975)),lty=2)
quantile(result_boot3_transformed,c(0.025,0.5,0.975))

1/(sqrt(1/(mean(match.data(m.out3,'control')[,45])+1))+quantile(result_boot3_transformed,c(0.025,0.5,0.975)))^2-1

step3=step(lrm3)
summary(step3)
step3_transformed=step(lrm3_transformed)
summary(step3_transformed)


treatment_group3=match.data(m.out3,'treat')
control_group3=match.data(m.out3,'control')
alldata3=match.data(m.out3)
for(i in 1:dim(treatment_group3)[2])
{ print(names(treatment_group3)[i])
  if(is.factor(treatment_group3[,i]))
  {test=fisher.test( table(alldata3$level1,alldata3[,i]))
  print(test$p.value)
  }
  else if((sd(control_group3[,i])!=0) & (sd(treatment_group3[,i])!=0))
  {
    test=t.test(treatment_group3[,i],control_group3[,i])
    print(test$p.value)
  }
  else
    print(1)
  
}

mean(fp_1year$events_1year[fp_1year$level1==1])
mean(fp_1year$events_1year[fp_1year$level1==0])

mean(fp_3year$events_3year[fp_3year$level1==1])
mean(fp_3year$events_3year[fp_3year$level1==0])
