programs_1yearindex=c(1,4,7,8,13,14,19,20,25:29)
programs_2yearindex=c(programs_1yearindex,2,5,9,10,15,16,21,22)
year1=cbind(features1,events1,programs1[,programs_1yearindex])
year2=cbind(features2,events2,programs2[,programs_2yearindex])
year3=cbind(features3,events3,programs3)

#1 year
year1_p1=sum(subset(year1,qa_Insp_Result=="Pass")$qa_upto_1yr_aftr_evnts>0)
table(subset(year1,qa_Insp_Result=='Pass')$qa_upto_1yr_prior_evnts,subset(year1,qa_Insp_Result=='Pass')$qa_upto_1yr_aftr_evnts)
year1_p0=sum(subset(year1,qa_Insp_Result=="Pass")$qa_upto_1yr_aftr_evnts==0)
table(subset(year1,qa_Insp_Result=='Fail')$qa_upto_1yr_prior_evnts,subset(year1,qa_Insp_Result=='Fail')$qa_upto_1yr_aftr_evnts)
table(subset(year1,qa_Insp_Result=='PassWithRecommendations')$qa_upto_1yr_prior_evnts,subset(year1,qa_Insp_Result=='PassWithRecommendations')$qa_upto_1yr_aftr_evnts)

year1_f1=sum(subset(year1,qa_Insp_Result=="Fail")$qa_upto_1yr_aftr_evnts>0)
year1_f0=sum(subset(year1,qa_Insp_Result=="Fail")$qa_upto_1yr_aftr_evnts==0)
year1_pwr1=sum(subset(year1,qa_Insp_Result=="PassWithRecommendations")$qa_upto_1yr_aftr_evnts>0)
year1_pwr0=sum(subset(year1,qa_Insp_Result=="PassWithRecommendations")$qa_upto_1yr_aftr_evnts==0)

#prior events vs after events
table(year1$qa_upto_1yr_prior_evnts,year1$qa_upto_1yr_aftr_evnts)

year1_prior1=sum(year1$qa_upto_1yr_prior_evnts>0)
year1_prior0=sum(year1$qa_upto_1yr_prior_evnts==0)
year1_after1=sum(year1$qa_upto_1yr_aftr_evnts>0)
year1_after0=sum(year1$qa_upto_1yr_aftr_evnts==0)

year2_prior1=sum(year2$qa_upto_1yr_prior_evnts>0)
year2_prior0=sum(year2$qa_upto_1yr_prior_evnts==0)
year2_after1=sum(year2$qa_upto_1yr_aftr_evnts>0)
year2_after0=sum(year2$qa_upto_1yr_aftr_evnts==0)

year3_prior1=sum(year3$qa_upto_1yr_prior_evnts>0)
year3_prior0=sum(year3$qa_upto_1yr_prior_evnts==0)
year3_after1=sum(year3$qa_upto_1yr_aftr_evnts>0)
year3_after0=sum(year3$qa_upto_1yr_aftr_evnts==0)



#Relative Risk
RR1=year1_after1/(year1_after0+year1_after1)/(year1_prior1/(year1_prior0+year1_prior1))
RR1_CI=log(RR1)+c(-1,1)*1.96*sqrt((1-year1_after1/(year1_after1+year1_after0))/year1_after1+(1-year1_prior1/(year1_prior1+year1_prior0))/year1_prior1)
exp(RR1_CI)

RR2=year2_after1/(year2_after0+year2_after1)/(year2_prior1/(year2_prior0+year2_prior1))
RR2_CI=log(RR2)+c(-1,1)*1.96*sqrt((1-year2_after1/(year2_after1+year2_after0))/year2_after1+(1-year2_prior1/(year2_prior1+year2_prior0))/year2_prior1)
exp(RR2_CI)

RR3=year3_after1/(year3_after0+year3_after1)/(year3_prior1/(year3_prior0+year3_prior1))
RR3_CI=log(RR3)+c(-1,1)*1.96*sqrt((1-year3_after1/(year3_after1+year3_after0))/year3_after1+(1-year3_prior1/(year3_prior1+year3_prior0))/year3_prior1)
exp(RR3_CI)