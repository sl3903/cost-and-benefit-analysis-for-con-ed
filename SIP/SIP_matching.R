matched <- matchit(level1 ~ neutral_mains + srvc_phase_cbls 
                  + all_events + all_major_events + last3year_events + Latitude
                  + Longitude + all_COpresent_events + last3year_COpresent_events
                  + last3year_major_events + all_COpresent_major_events
                  + nwk_count + propensity_score,
                  data = fp_1year, method = "nearest", distance = "mahalanobis",
                 reestimate=TRUE, replace=TRUE)

save.image("F:/My Documents/SIP/SIP_matching.RData")



sum(fp_3year$level1[complete.cases(fp_3year)]==1)

# fp_1year=fp_1year[,-c(5:8,21:22)]
# fp_3year=fp_3year[,names(fp_1year)]
# fp_1year=fp_1year[,-46]
# fp_3year=fp_3year[,-45]

#fml=as.formula(paste("level1~",paste(names(fpe)[1:44],collapse = "+")))

for( i in 1:dim(fp_1year)[2])
{print(names(fp_1year)[i])
  print(sum(!is.na(fp_1year[,i]),na.rm=T))}
sum(complete.cases(fp_1year))

means1=c()
sds1=c()
name1=c()
for(i in 1:dim(fp_1year)[2])
{
  if(!is.factor(fp_1year[,i]))
    if(!(sum(range(fp_1year[,i],na.rm = T)==c(0,1))==2))
  {name=append(name,names(fp_1year)[i])
      means=append(means,mean(fp_1year[,i],na.rm = T))
  sds=append(sds,sd(fp_1year[,i],na.rm = T))
      fp_1year[,i]=(fp_1year[,i]-mean(fp_1year[,i],na.rm = T))/sd(fp_1year[,i],na.rm = T)
    # print(names(fp_1year)[i])
    # print(mean(fp_1year[,i],na.rm = T))
    # print(sd(fp_1year[,i],na.rm = T))
    # print(range(fp_1year[,i],na.rm = T)) 
    }
  
  
}


means3=c()
sds3=c()
name3=c()
for(i in 1:dim(fp_3year)[2])
{
  if(!is.factor(fp_3year[,i]))
    if(!(sum(range(fp_3year[,i],na.rm = T)==c(0,1))==2))
    {name3=append(name3,names(fp_3year)[i])
    means3=append(means3,mean(fp_3year[,i],na.rm = T))
    sds3=append(sds3,sd(fp_3year[,i],na.rm = T))
    fp_3year[,i]=(fp_3year[,i]-mean(fp_3year[,i],na.rm = T))/sd(fp_3year[,i],na.rm = T)
    # print(names(fp_1year)[i])
    # print(mean(fp_1year[,i],na.rm = T))
    # print(sd(fp_1year[,i],na.rm = T))
    # print(range(fp_1year[,i],na.rm = T)) 
    }
  
  
}

#fp_1year=fp_1year[complete.cases(fp_1year),]
#fp_3year=fp_3year[complete.cases(fp_3year),]

library(MatchIt)

library(Zelig)

library(boot)

fml=as.formula(paste("level1~",paste(names(fp_1year)[1:43],collapse = "+")))

m.out1 <- matchit(fml, data = fp_1year[complete.cases(fp_1year),1:45], method = "nearest",reestimate=TRUE, replace=TRUE)

m.out3 <- matchit(fml, data = fp_3year[complete.cases(fp_3year),c(1:45)], method = "nearest",reestimate=TRUE, replace=TRUE)

summary(m.out1)

summary(m.out1,interaction=T,addlvariables=NA, standerdize=T)

plot(m.out1)

plot(m.out1,type='jitter')

plot(m.out1,type='hist')

fml_te1=as.formula('(events_1year ) ~ StructureType + Borough + Latitude + 
    Longitude + out_cables + in_cables + total_cables + oldest_cable_built_date + 
                   total_cu + total_al + neutral_mains + main_phase_cbls + srvc_phase_cbls + 
                   incidentborough + latestincidentdate + TroubleType + nwk_count + 
                   COPresent + all_events + all_COpresent_events + lastyear_events + 
                   lastyear_COpresent_events + last3year_events + last3year_COpresent_events + 
                   all_major_events + all_COpresent_major_events + lastyear_major_events + 
                   lastyear_COpresent_major_events + last3year_major_events + 
                   last3year_COpresent_major_events + Total_MH + Vented_MH + 
                   QA_program + MTH_program + vented_cover + Mobile_test_program + 
                   latestincidenttime + latestincidentyear + latestincidentmonth + 
                   latestincidentday + ocb_year + ocb_month + ocb_day + level1')

fml_te1_boxcox=as.formula('(events_1year+1) ~ StructureType + Borough + Latitude + 
                   Longitude + out_cables + in_cables + total_cables + oldest_cable_built_date + 
                               total_cu + total_al + neutral_mains + main_phase_cbls + srvc_phase_cbls + 
                               incidentborough + latestincidentdate + TroubleType + nwk_count + 
                               COPresent + all_events + all_COpresent_events + lastyear_events + 
                               lastyear_COpresent_events + last3year_events + last3year_COpresent_events + 
                               all_major_events + all_COpresent_major_events + lastyear_major_events + 
                               lastyear_COpresent_major_events + last3year_major_events + 
                               last3year_COpresent_major_events + Total_MH + Vented_MH + 
                               QA_program + MTH_program + vented_cover + Mobile_test_program + 
                               latestincidenttime + latestincidentyear + latestincidentmonth + 
                               latestincidentday + ocb_year + ocb_month + ocb_day + level1')


fml_te1_transformed=as.formula('(events_1year+1)^(-2) ~ StructureType + Borough + Latitude + 
                   Longitude + out_cables + in_cables + total_cables + oldest_cable_built_date + 
                   total_cu + total_al + neutral_mains + main_phase_cbls + srvc_phase_cbls + 
                   incidentborough + latestincidentdate + TroubleType + nwk_count + 
                   COPresent + all_events + all_COpresent_events + lastyear_events + 
                   lastyear_COpresent_events + last3year_events + last3year_COpresent_events + 
                   all_major_events + all_COpresent_major_events + lastyear_major_events + 
                   lastyear_COpresent_major_events + last3year_major_events + 
                   last3year_COpresent_major_events + Total_MH + Vented_MH + 
                   QA_program + MTH_program + vented_cover + Mobile_test_program + 
                   latestincidenttime + latestincidentyear + latestincidentmonth + 
                   latestincidentday + ocb_year + ocb_month + ocb_day + level1')

bootstrap=function(treatment_group,control_group,formular)
{ 
  t=sample(dim(treatment_group)[1],dim(treatment_group)[1],replace = T)
  c=sample(dim(control_group)[1],dim(control_group)[1],replace = T)
  data=rbind(treatment_group[t,],control_group[c,],rep(c(1,0),level1=c(dim(treatment_group)[1],dim(control_group)[1])))
  lrm=lm(formular,data = data)
  print(lrm$coefficients[length(lrm$coefficients)])
  lrm$coefficients[length(lrm$coefficients)]
}
n_boot=1000
result_boot1=c()
result_boot1_transformed=c()
for(i in 1:n_boot)
{
  result_boot1[i]=bootstrap(match.data(m.out1,'treat'),match.data(m.out1,'control'),fml_te1)
  result_boot1_transformed[i]=bootstrap(match.data(m.out1,'treat'),match.data(m.out1,'control'),fml_te1_transformed)
  
}

lrm1=lm(fml_te1,data = match.data(m.out1))
summary(lrm1)
hist(residuals(lrm1),xlab = 'residuals',main='Residual Plot')
qqnorm(residuals(lrm1),main = 'QQplot of the Residuals')
qqline(residuals(lrm1))


boxcox(lm(fml_te1_boxcox,data = match.data(m.out1)))
lrm1_transformed=lm(fml_te1_transformed,data = match.data(m.out1))
summary(lrm1_transformed)

hist(result_boot1,xlab = 'treatment effect',main = 'Histogram of Treatment effect')
abline(v=quantile(result_boot1,c(0.025,0.975)),lty=2)
quantile(result_boot1,c(0.025,0.5,0.975))

hist(result_boot1_transformed,xlab = 'treatment effect',main = 'Histogram of Transformed Treatment effect')
abline(v=quantile(result_boot1_transformed,c(0.025,0.975)),lty=2)
quantile(result_boot1_transformed,c(0.025,0.5,0.975))

1/(sqrt(1/(mean(match.data(m.out1,'control')[,45])+1))+quantile(result_boot1_transformed,c(0.025,0.5,0.975)))^2-1

step1=step(lrm1)
summary(step1)
step1_transformed=step(lrm1_transformed)
summary(step1_transformed)

treatment_group1=match.data(m.out1,'treat')
control_group1=match.data(m.out1,'control')
alldata1=match.data(m.out1)
for(i in 1:dim(treatment_group1)[2])
{ print(names(treatment_group1)[i])
  if(is.factor(treatment_group1[,i]))
  {test=fisher.test( table(alldata$level1,alldata[,i]))
    print(test$p.value)
  }
  else if((sd(control_group1[,i])!=0) & (sd(treatment_group1[,i])!=0))
  {
    test=t.test(treatment_group1[,i],control_group1[,i])
    print(test$p.value)
  }
  else
    print(1)
  
}
